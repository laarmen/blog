+++
title = "Programming school need to teach communication"
date = 2020-07-14
draft = true

[taxonomies]
tags = ["rant"]
+++

# Formal education on software development misses the point

What I remember of the various Programming and Software engineering courses I
was taughe are mostly things like introduction to OOP, functional programming,
and, weirdly, an introduction to the proof system "D" (no relations to the init
system). The focus was mostly on the various ways to make the computer do what
we want it to do, and also on how to prove that our instructions means what we
think they mean.[^digression]

In any case, whatever the focus of the classes, they all centered on the idea
of writing a program. The assignment were usually writing self-contained units
of code, (whole programs, classes, or single functions), and the deliverable
was the source code of said unit.

There were no notion of managing changes to a codebase, nor how to actually
collaborate to produce a piece of software. To be fair, I dropped out of
university after a bachelor's degree, those parts might have been mentioned in
the master's degree classes, but that's IMO far too late, they could very well
have swapped thoses classes for the quantum physics or linear algebra, both of
which seem more useful at an advanced degree.

[^digression]: In one instance, the curriculum was "Ocaml is a great language", "types are
cool", "C isn't a programming language but a vaguely portable assembly language"
and "we have to teach you C++ because that's what the industry uses besides
Java, but it's awful". Granted, it was in a school aiming at making CS
researchers more than software engineers, which explains the final examination
: we had to write a general plan for a 1-hour seminar for "3rd year Math
students" on either the class system for C++ or something related to Ocaml.
That was the best exam form I've ever seen.

Of course that was also one of the worst examples of confirmation bias in teaching
I'd ever seen, especially C11 and C++11 already existed, and C++14 was well on its way.
But since one cannot (and should not) rewrite everything that's ever written to
use the newest and greatest features of the languages, one could surely pretend that
those new standards don't exist, right? :-)
