+++
title = "Git: it's all about blame"
date = 2020-07-14
draft = false

[taxonomies]
tags = ["git", "workflow"]
+++

This post is the first of a series[^famous_last_words] about how I use Git, and
why I consider it a crucial, central tool in my developer toolbox. As it seems like
most Git documentation focuses on the creation and transformation of a Git history, I'll
start with how I actually *use* this history.

<!-- more -->

***DISCLAIMER***: I made up the Python code samples, no white rabbit has been hurt
during production of this blog post.

# Case study

Your current project is a website with a lot of complex features interacting in
arcane ways. The codebase is more than a decade old and has seen at least three refactors.
You know this sort of project. Most developers will at least once in their career see
projects like this.

One of the feature is a text publishing module tying posts to products. The
posts can have three status: `DRAFT`, `PUBLISHED` and deleted. The deleted
status had never been used until yesterday, when your company had to pull a
post for legal reasons, and it turns out the deleted item still shows up in the
global index.

Digging into the code, you find the following function.

```python
def buildIndex(post):
    if post.status == DRAFT:
        prepare_draft(post)
    else:
        prepare_published(post)

    buildIndexHelper(post)
```

It's clear that this function does **not** take the `DELETED` case into account.
Your fix is straightforward, and looks like the following diff.
We'll leave the implementation of `prepare_deleted` to the imagination.

```diff
         prepare_draft(post)
+    elif post.status == PUBLISHED:
+        prepare_published(post)
+    elif post.status == DELETED:
+        prepare_deleted(post)
     else:
-        prepare_published(post)
+        raise Exception("Unsupported status")

```

If you check `buildIndexHelper`, you notice that it already has code
handling the `DELETED` case:

```python
def buildIndexHelper(post):
    doNormalStuff(post)
    # Handle the Carroll special case in its own thread since it is heavy
    # computation.
    if post.status == "DELETED" and post.author.startswith("Lewis"):
        spawn(lambda: goDownTheRabbitHole(post.clone()))
    doMoreNormalStuff(post)
end
```

Evidently, this conditional was dead code until now. The comment could be
helpful, in that it hints at some peculiar context, and tells you that whatever
`goDownTheRabbitHole` does, it seemed significant enough to warrant special
treatment. However, since this code was dead so far, there are no guarantees
that it still works as intended. Furthermore, we don't actually know *why* Mr
Carroll needed a special treatment, nor if such treatment is still warranted.

You *could* go down the rabbit hole and read the code of the called function,
which might be short, simple, and well documented. Or it might be a 200-lines
blob of nested `while` loops, using one-letter variables, and calling out to
C libraries.

Or you could do as I would.

# Git blame

Git has a notoriously confusing UI on the command line, and `git blame` is one
example among a lot of somewhat inadequatly named commands. Of course the
majority of the alternative UIs out there use the same terminology for the sake
of consistency, although others call it "Annotate".

To "blame" a file is to identify, for each line of the file, the commit that
last touched it. You can see below two interfaces for such blame, one from
GitHub, the other one from the `fugitive` plugin for Vim.

![GitHub blame on a Zola source file](blame_github.png)
![Vim blame on a Zola source file](blame_vim.png)

If you look at the [blame for our
functions](https://gitlab.com/laarmen/blog-repo/-/blame/blame_post/index.py),
you'll note that our special case has been added by John Smith, which is already a
valuable information : if they are still around, you can always ask them what
it's all about.

However, this happened 5 years ago, which means there is a decent change John
might have forgotten the details of the problem this patch solved, and that's
assuming they're still employed at EvilCorp. If they had a good Git
hygiene[^git_hygiene] the needed information might actually be recorded within
the commit itself.

```gitlog
commit 23e0744de4c63033bd0edbe1ef3de959660b6c78
Author: John Smith <j.smith@evilcorp.com>
Date:   Sat Feb 7 18:27:45 2015 +0100

    Force a rabbit hole call for all Lewises
    
    The government has issued a new law, championed by the senator Carroll
    from California, mandating us to compile all the interactions anyone had
    with the user named Lewis anytime they remove a product from their
    store and send them to the Rabbit Hole company for processing.
    
    As this is a very heavy process, it's run in its own thread in order not
    to block the main thread.

diff --git a/index.py b/index.py
index 8ccedb7..7bcd7f4 100644
--- a/index.py
+++ b/index.py
@@ -10,4 +10,8 @@ def buildIndex(post):
 
 def buildIndexHelper(post):
     doNormalStuff(post)
+    # Handle the Carroll special case in its own thread since it is heavy
+    # computation.
+    if post.status == DELETED and post.author.startswith("Lewis"):
+        spawn(lambda: goDownTheRabbitHole(post.clone()))
     doMoreNormalStuff(post)
```

Indeed, looking at the full commit tells us that this patch is because of a
change in the wider world. The event here is of a legal nature, but it could as
well have been a workaround for a client's specific requirements. A quick check
could tell you whether this law is still valid, or if this specific client is a
*current* client of EvilCorp.

# Commit logs or code comments ?

A common objection to writing detailed commit logs is that they're redundant
with code comments next to the relevant code. If you feel that this observation
rings true, I want you to consider [this
file](https://github.com/torvalds/linux/blob/5643a552d31242e5adf3dbefba26c90b1bce2826/drivers/net/tap.c)
from the Linux kernel. The source is decently commented compared to the rest of
the codebase – I'd still prefer a bit more comments personally. However, have a
look at its commit history. The commits there contain *at least* one
paragraph besides the shortlog. If each piece of code had the content of the last
commit that touched it next to it, the size of the code would explode, making
it hard to understand as the reader would be bombarded with information.

More importantly, comments tend to go stale as we shuffle the code around.
This unerlines perhaps the single most important property of a comment in a git
commit: it has a date and author, and is linked to a transition from one
immutable state to another. There is no doubt about which specific bit of code
the comment is about, and said code *cannot* grow out of sync with the
comment.

In another, smaller point, look at [this
commit](https://github.com/torvalds/linux/commit/dea6e19f4ef746aa18b4c33d1a7fed54356796ed):
in which file would you put your inline comment? A change that is logically
atomic might touch a lot of different files.

# It's not *only* about blame

One other reason you should care about commit messages is code review. GitHub
and Gitlab will actually pick up the full text of your commit if you open a
PR/MR with a single commit, making the process much faster. Other tools might
even allow you to only apply some commits but not others, in which case it helps to
give the precise reasons why you wrote a particular commit.

Describing a change and the reasons behind it is also a great way to sanity-check your
changes. Writing out those things in a commit log in plain English (or any natural language
you're comfortable with) means that you should be able to explain them to whomever will
review your code and merge in your changed.

[^famous_last_words]: Famous last words...

[^git_hygiene]: The notion of Git hygiene varies from person to person. For this case,
I mean "the author makes atomic commits with a proper documentation describing not only
the *what* but also the *why*". More on this in later posts.
